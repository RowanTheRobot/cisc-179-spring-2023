import sys

with open(
    "deras.txt",
    encoding="utf-8",
) as f:
    # GOOD: I like your human-readable, machine-friendly "syntax" for your file.
    room_descr = f.read().split("---")


def exit_game():
    print("Game over!")
    print("****************************************************")
    restart = input("Would you like to try again? (Yes/No) ")
    # GOOD: I like your case-insensitive string processing to make commands forgiving
    # TODO: check just the first letter of yes/no questions
    if restart.lower().strip() == "yes":
        start_game()
    else:
        sys.exit()


def start_game():
    choose = input("Would you like to go on an adventure? (Yes/No) ")
    if choose.lower().strip() == "yes":
        # TODO: rather than trying to memorize numerical indexes for all the rooms, use dict keys to give them names
        print(room_descr[0])
    else:
        print(room_descr[1])
        exit_game()

    choose = input(room_descr[24])
    # TODO: Instead of a, b options, try to use natural words that are part of the story
    # TODO: Use choose.lower().strip()[0] to check only the first letter of a word like "beach" "jungle" or "ruins"
    if choose.lower().strip() == "a":
        print(room_descr[2])
        choose = input(room_descr[25])
        if choose.lower().strip() == "a":
            print(room_descr[3])
            choose = input(room_descr[26])
            if choose.lower().strip() == "a":
                print(room_descr[4])
                # TODO: Answering the exit/restart quesiton repeatedly gets annoying
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[5])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        elif choose.lower().strip() == "b":
            print(room_descr[6])
            choose = input(room_descr[27])
            if choose.lower().strip() == "a":
                print(room_descr[7])
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[8])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        else:
            print(room_descr[23])
            exit_game()
    elif choose.lower().strip() == "b":
        print(room_descr[9])
        choose = input(room_descr[28])
        if choose.lower().strip() == "a":
            print(room_descr[10])
            choose = input(room_descr[29])
            if choose.lower().strip() == "a":
                print(room_descr[11])
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[12])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        elif choose.lower().strip() == "b":
            print(room_descr[13])
            choose = input(room_descr[30])
            if choose.lower().strip() == "a":
                print(room_descr[14])
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[15])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        else:
            print(room_descr[23])
            exit_game()
    elif choose.lower().strip() == "c":
        print(room_descr[16])
        choose = input(room_descr[32])
        if choose.lower().strip() == "a":
            print(room_descr[17])
            choose = input(room_descr[31])
            if choose.lower().strip() == "a":
                print(room_descr[18])
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[19])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        elif choose.lower().strip() == "b":
            print(room_descr[20])
            choose = input(room_descr[33])
            if choose.lower().strip() == "a":
                print(room_descr[21])
                exit_game()
            elif choose.lower().strip() == "b":
                print(room_descr[22])
                exit_game()
            else:
                print(room_descr[23])
                exit_game()
        else:
            print(room_descr[23])
            exit_game()
    else:
        print(room_descr[23])
        exit_game()


start_game()
