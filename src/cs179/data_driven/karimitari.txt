wakeup:You come to in a dimly lit room.  A candle on a table provides the only light in the room.
wakeupAgain:You come to in a dimly lit room.  A candle on a table provides the only light in the room. The room seems familiar.  You wonder if you have been here before.
whatToDo:You look around and notice you are in the center of the room, and there are four doors equidistant apart. It seems like the only way out is through one of them.
doorMessage:You can pick a door to the north, south, east, or west.  Which door would you like to choose?
doorLocked:The door appears to be locked.  The handle will not open it. There is a keyhole, so perhaps a key would unlock this door.
pullOutKey:You pull out your key and decide to try it in the lock.
keyDoesNotWork:This key does not fit in this lock.
keyWorks:It worked! The key fits and you hear a click.
doorStillLocked:The door is still locked, so maybe you should try another door.
roomTwo:Upon entering, you can barely see.  Only a small candle again lights the room, but you can tell there are no other doors here.
roomTwoWithoutKey:You quickly turn and go back the way you came.
roomTwoWithKey:You decide to look in here again, and now you notice that a ladder is hanging from the ceiling. How strange...
roomTwoWithKeyCont:Nothing ventured, nothing gained.  You climb the ladder and hit your head sooner than you expect.  Looking up, you notice a trapdoor with a keyhole.  Can you reach into your pocket for the key?
secondToLastRoom:Wow, if this is not the way out, you are in trouble. The room smells better than the last one, so there must be a way out.  \nYour eyes have to adjust because there is no candle or other light source except dimly by the far wall... there is a window! And you can make out a door!  You rush over to the door and clasp the handle.
escape:You swing the door outward and the smell of night hits your nose and the air rushes into your lungs.  \nYOU MADE IT OUTSIDE! \nCongratulations! Looking below, it seems like you are at the top of a castle. Now, you just have to figure out how to get down...
roomThree:This room has a torch on the wall, nearly blinding you as you come inside after the dimly lit chamber you were just inside.
roomThreeContinued:After your eyes adjust, you can clearly tell there is nothing of note.  As you turn to leave, you catch a glint in the corner.
roomThreeFinish:You walk over and reach down to pick up a key.  Maybe this will will open a door.
roomThreeAlreadyHaveKey:You already got the key from here and there is nothing else to do.  You go back.
roomFour:This room is cozy.  There is a sofa and a fireplace with a lit fire.  This looks promising.
roomFourAfterDied:Actually, the fireplace looks scary, and there is a creepy drapdoor.
goBackToFirstRoom:This does not appear to be the way out. You go back to the first room.
whatToDoNext:Which door do you want to try next?  You can pick a door to the north, south, east, or west.  Which door would you like to choose?
trapDoorMessage:There is a trapdoor that does not appear to be locked.  Do you want to try going down there?
trapDoorMessageYes:You open the hatch and descend some steep stairs.
trapDoorMessageNo:You decide not to see what is down below.
haveDiedBeforeMessage:On second thought, that trapdoor looks suspicious.  You should probably avoid it.
basementMessage:Uh oh. You feel like this was a mistake.  Two red eyes blink open in the darkness of the room you climbed down into, and you hear a low growl.  Quick! You only have seconds, climb the stairs!
basementDeath:Too late.  The monster had his jaws around you before you could even turn around.  You hear bones crunching before you lose consciousness, as a white light shines hazily in front of your eyes.
goBackMessage:Do you want to go back the way you came?
yesOrNo:Type y(es) or n(o) (or any word beginning with y or n will be interpreted as yes or no).
notYesOrNo:You must type y(es) or n(o).
roomChecked:You already checked that room. You should try another room.
checkAgain:Well, you have checked all the rooms. Maybe one of the three open rooms has something you missed...