# This is how the dictionary looks after being read and generated from the data file
# dictionary = {          
#                "start":{"explanation text":"Before you begin your long and difficult trek to California, you have about $50 to spare for extra equipment",
#                         "input text":"If you would like to continue, press 'y', otherwise, press any key. ",
#                         "y":["Alright, let's start the journey...","equipment","equipment","equipment"],
#                         "else":["That's ok, it isn't for the faint of heart...","end","end","end"]
#                         },
               
               
#                "equipment":{"explanation text":"\nBefore you begin your long and difficult trek to California, you have about $50 to spare for extra equipment.",
                            
#                             "input text":"\nIf you would like to buy extra winter clothes for the journey press 'y'.\nIf you would like to buy extra weapons press 'x'. ",
#                             "y":["clothes","indians","indians","indians"],
#                             "x":["weapons","indians""indians","indians"],
#                             "else":["Invalid entry. Ending the game...","end","end","end"]
#                             },
               
               
#                "indians":{"explanation text":"\n5 days after you set out on your journey, you are approached by indians who want to have you stay with  them and trade for a few days.",
#                           "input text":"\nIf you want to stay and trade, press 'y'. \nIf you do not want to stop and continue on your journey faster, press 'x'.",
#                           "y":["","crossing","crossing","crossing"],
#                           "x":["","robbers","robbers","robbers"],
#                           "else":["Invalid entry. Ending the game...","end","end","end"]
#                           },
                       
               
               
#                "crossing":{"explanation text":"\nOut of hospitality, the indians give you food and tips to help you survive (P.S. cross the mountains while the weather is good).",
                
#                            "input text":"\nYou are able to reach the Kansas River after 15 days but the river looks a bit swollen. If you want to cross, press 'y'. \nIf you want to wait, press 'x' ",
                           
#                            "y":["You drown while trying to cross the river...","end","end","end"],
#                            "x":["You are able to cross after 5 days!", "sierra60","sierra60","sierra60"],
#                            "else":["Invalid entry. Ending the game...","end","end","end"]
#                            },
                           

#                "sierra60":{"explanation text":"",
                           
#                            "input text":"After 60 days on the trail, the weather in the Sierras doesn't look the best. \nIf you want to cross now, press 'y'. If you want to wait more, press 'x'. ",
#                            "y":["", "freeze","win","win"],
#                            "x":["", "final","final","final"],
#                            "else":["Invalid entry. Ending the game...","end","end","end"]
#                           },
               
               
#                "final":{"explanation text":"After traveling for 70 days, the weather doesn't look any better. In fact, it looks worse!",
#                         "input text":"If you want to continue waiting, press 'y'.\nIf you want to try to cross, press 'x'.",
#                         "y":["There is never a window of good weather and you die of starvation in the Mojave Desert...","end","end","end"],
#                         "x":["","win","win","win"],
#                         "else":["Invalid entry. Ending the game...","end","end","end"]
#                         },
               
               
#                "robbers":{"explanation text":"\nYou were able to reach and cross the Kansas River after 10 days of traveling. \nAs you continue on your journey, you are confronted by bandits trying to take all of your belongings.\n",

#                           "input text":"If you want to fight back against the bandits, press 'y'.\nIf you want to let them have your belongings, press 'x'. ",
#                           "y":["You try to fight back but you are killed by the bandits. They outnumbered you...","end","end","end"],
#                           "x":["You give them your belongings and die of starvation","end","end","end"],
#                           "else":["Invalid entry. Ending the game...","end","end","end"]
#                           },
               
               
#                "win":{"explanation text":"CONGRATULATIONS!!! YOU WON THE GAME AND REACHED CALIFORNIA!!!",
#                       "input text":"",
#                       "else":["","end","end","end"]
#                      },
               
#                "freeze":{"explanation text":"You freeze to death while crossing the Sierras due to a lack of clothing...",
#                       "input text":"",
#                       "else":["","end","end","end"]
#                      },
                   
#                }

# loads the data to a dictionary from the file "shaheen.txt"
def createfromfile():
    
    dictionary ={}

    f = open("shaheen.txt")
    
    x = f.readline()
    while x != "": 
        subdictionary = {}
    #line1 State
        if x != "" and x[0] == "#" and x.replace("\n","") != "empty":
            state = x.replace("#","").replace("\n","")   
        
    #line2 explanation text
        x = f.readline()
        if x != "" and x.replace("\n","") != "empty":
            z = x.split(":")
            subdictionary[z[0]]=z[1].replace("\n","")  
            dictionary [state] = subdictionary

    #line3 input text
        x = f.readline()
        if x != "" and x.replace("\n","") != "empty":
            z = x.split(":")
            subdictionary[z[0]]=z[1].replace("\n","")  
            dictionary [state] = subdictionary
    
    #line4 choice
        x = f.readline()
        if x != "" and x.replace("\n","") != "empty":
            z = x.split(":")
            w = z[1].replace("\n","").split(";")
            
            dictionary [state][z[0]]= w

    #line5 choice
        x = f.readline()
        if x != "" and x.replace("\n","") != "empty":
            z = x.split(":")
            w = z[1].replace("\n","").split(";")
    
            dictionary [state][z[0]]=w
            
    #line6 choice
        x = f.readline()
        if x != "" and x.replace("\n","") != "empty":
            z = x.split(":")
            w = z[1].replace("\n","").split(";")
    
            dictionary [state][z[0]]=w
        x = f.readline()

    f.close()
    return dictionary

# The function checks if the state text is in the dictionary
def isindictionary(dictionary, state, intext):
    isin = False
    
    keys = dictionary[state].keys()
    if (intext in keys):
        isin = True
    return isin


# The main function for running the game
def app ():
    state = "start"
    weapons = False
    clothes = False
    option = ""

    dictionary = createfromfile()    
    
    name = input ("What would you like to be called?")
    print ("Nice to meet you, ", name,"! Welcome to Independence, Missouri!")
    
        
    while state != "end":
        print (dictionary[state]["explanation text"])
        
        if (dictionary[state]["input text"] != ""):
            intext = input (dictionary[state]["input text"])

        if isindictionary(dictionary, state, intext):
             option = intext
        else:
             option = "else"
         
        if dictionary[state][option][0]=="weapons":
              weapons = True
        elif dictionary[state][option][0]=="clothes":
              clothes = True
        print (dictionary[state][option][0])

        if weapons == True:
             state = dictionary[state][option][2]
        elif clothes == True:
             state = dictionary[state][option][3]
        else:
             state = dictionary[state][option][1]   
                                     
    raise SystemExit()

if __name__ == '__main__':    
    app()
