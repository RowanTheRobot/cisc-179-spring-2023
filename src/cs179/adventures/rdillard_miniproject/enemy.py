import constants as c
import random as rand


class Enemy:
	def __init__(self, kind=None, health=None, damage=None):
		self.kind = kind
		self.health = health
		self.damage = damage

	def first_attack(self):
		die_roll = rand.uniform(0.5, 1.05)
		damage = int(self.damage * die_roll)
		print(f"{c.VIOLET}{self.kind}{c.RESET} will deal {damage} damage, if it's not prevented by the {c.YELLOW}{c.SHIELD}{c.RESET}.\n")
		return damage

	def second_attack(self):
		die_roll = rand.uniform(0.1, 2.0)
		damage = int(self.damage * die_roll)
		print(f"{c.VIOLET}{self.kind}{c.RESET} will deal {damage} damage, if it's not prevented by the {c.YELLOW}{c.SHIELD}{c.RESET}.\n")
		return damage

	def start_third_attack(self):
		print(f"{c.VIOLET}{self.kind}{c.RESET} is charging {c.RED}{c.FINISH_THIRD_ATTACK}{c.RESET}.\n")
		return 0

	def finish_third_attack(self):
		die_roll_one = rand.uniform(0.1, 2.0)
		die_roll_two = rand.uniform(0.5, 3.0)
		die_roll_three = rand.uniform(0.7, 5.0)
		damage = int(self.damage * die_roll_one * die_roll_two * die_roll_three)
		print(f"{c.VIOLET}{self.kind}{c.RESET} will deal {damage} damage, if it's not prevented by the {c.YELLOW}{c.SHIELD}{c.RESET}\n")
		return damage
