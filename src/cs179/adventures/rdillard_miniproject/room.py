import constants as c


class Room:
	def __init__(self, dirs, items, description, rooms=None):
		self.dirs = dirs
		self.items = items
		self.description = description
		self.connected_rooms = rooms

	def connect_rooms(self, rooms_to_add):
		for room in rooms_to_add:
			if self.connected_rooms is None:
				self.connected_rooms = []
			self.connected_rooms.append(room)

	def print_room(self):
		print(f"{c.GREEN}{self.description[0]}:{c.RESET}")
		print(self.description[1].replace('. ', '.\n'))
		if self.connected_rooms:
			print(f"\n{c.RED}The following rooms can be reached from here:{c.RESET}")
			for cr in self.connected_rooms:
				print(f"{c.BLUE}{cr[0]}: {cr[1].description[0]}{c.RESET}")
