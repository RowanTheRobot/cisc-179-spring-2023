import random
import constants as c


class Item:
	def __init__(self, item_type, name, damage=0, defense=0, charges=1, active=False):
		self.item_type = item_type
		self.name = name
		self.damage = damage
		self.defense = defense
		self.charges = charges
		self.active = active

	def use_weapon(self):
		#	Damage multiplier: a float between 0 and 1.2, where 1.2 allows for up to a 20% critical hit bonus.
		die_roll = random.uniform(0.0, 1.2)

		#	Light Arrows run on charges. They're displayed where they matter.
		if self.charges <= 0:
			print(f"You don't have enough charges.")
			return 0
		if self.name == c.LIGHT_ARROW:
			self.charges -= 1
			if self.charges <= 0:
				print(f"You've used your final charge for the {self.name}.")
				self.active = False
		damage_done = int(self.damage * die_roll)
		return damage_done

	def use_defense(self):
		#	Defense multiplier: a float between 0 and 1.2, where 1.2 allows for up to a 20% chance to prevent all damage.
		die_roll = random.uniform(0.0, 1.2)
		damage_prevented = int(self.defense * die_roll)

		#	The die roll was sufficiently low
		if damage_prevented == 0:
			print(f"{c.YELLOW}{c.SHIELD}{c.RESET} couldn't perform any defensive measures this time...")
			damage_prevented = 1

		#	Prevent all damage
		elif damage_prevented >= 50:
			print(f"Rolled higher than 50 damage prevention.")

		#	Damage reduction
		else:
			print(f"{c.YELLOW}{c.SHIELD}{c.RESET} will divide the damage by {c.RED}{damage_prevented}{c.RESET}.")
		return damage_prevented
