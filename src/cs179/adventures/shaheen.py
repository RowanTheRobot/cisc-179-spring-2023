import random

# function that generates a 50/50 chance


def chance():
    return random.randrange(2)


def options(idx):
    """ print a statement from a 2-D matrix

    the first section prints a statement and the second provides an input text
    the input parameter idx is the index of the explanation text list
    """

    explanationtext = [
        # 0 User name
        ["",
         "What would you like to be called?"],

        # 1 Background and game start
        ["Welcome to the Oregon Trail!\n\nThe year is 1847. You"
         " will be attempting the tough journey West to try and"
         " start a new life.\nThe journey will be difficult and"
         " you may have to avoid drowning in rivers, crossing "
         " mountains, starvation, and other threats.\n",

         "If you would like to continue, press 'y', otherwise,"
         " press any key. "],

        # 2 Purchase equipment
        ["\nBefore you begin your long and difficult trek to"
         " California, you have about $50 to spare for extra"
         " equipment.",

         "\nIf you would like to buy extra winter clothes for"
         " the journey press 'y'.\nIf you would like to buy"
         " extra weapons press 'x'. "],

        # 3 Approached by indians
        ["\n5 days after you set out on your journey, you are"
         " approached by indians who want to have you stay with "
         " them and trade for a few days.",

         "\nIf you want to stay and trade,"
         " press 'y'. \nIf you do not want to stop and continue on"
         " your journey faster, press 'x'. "],

        # 4 Kansas River crossing decision
        ["\nOut of hospitality, the indians give you food and"
         " tips to help you survive (P.S. cross the mountains"
         " while the weather is good).",

         "\nYou are able to reach the Kansas River after 15 days"
         " but the river looks a bit swollen. If you want to"
         " cross, press 'y'. \nIf you want to wait, press 'x' "],

        # 5 Sierra Nevada after 60 days
        ["",

         "After 60 days on the trail, the weather in the"
         " Sierras doesn't look the best. \nIf you want"
         " to cross now, press 'y'. If you want to wait more,"
         " press 'x'. "],

        # 6 Sierra Nevada after 70 days
        ["After traveling for 70 days, the weather doesn't look"
         " any better. In fact, it looks worse!",

         "If you want to"
         " continue waiting, press 'y'.\nIf you want to try to"
         " cross, press 'x'. "],

        # 7 Robbers on the trail
        ["\nYou were able to reach and cross the Kansas River"
         " after 10 days of traveling. \n"
         "As you continue on your journey, you are confronted"
         " by bandits trying to take all of your belongings.\n",

         "If you want to fight back against the bandits, press"
         " 'y'.\nIf you want to let them have your belongings,"
         " press 'x'. "],

        # 8 Sierra Nevada after 50 days
        ["You were able to reach the Sierra Nevada Mountains"
         " after 50 days on the trail.\n",

         "The weather at the foot of the mountains doesn't"
         " look too bad, but you heard rumors from other"
         " travelers that there is a storm brewing on the top."
         " \nIf you want to continue the journey up the mountain"
         " and into California, press 'y'. If you want to wait,"
         " press 'x'. "]]

    print(explanationtext[idx][0])
    option = input(explanationtext[idx][1])
    return option


# This function prints a statement based on the provided parameter and
# ends the game
def endgame(parameter):

    endtext = [
        # 0 Called when the user doesn't want to play
        "That's ok, it isn't for the faint of heart...",
        # 1 Called when crossing the river unsuccessfully
        "You drown while trying to cross the river...",
        # 2 Called when you give all of your belongings to the robbers
        "You give them your belongings and die of starvation...",
        # 3 Called when winning the game
        "CONGRATULATIONS!!! YOU WON THE GAME AND REACHED CALIFORNIA!!!",
        # 4 Called when crossing the Sierra Nevada unsuccessfully
        "You freeze to death while crossing the Sierras due to a lack of "
        "clothing...",
        # 5 Called when not crossing the Sierra Nevada in time
        "There is never a window of good weather and you die of starvation"
        "in the Mojave Desert...",
        # 6 Called when you lose the fight to the robbers
        "You try to fight back but you are killed due to a lack of weapons...",
        # 7 Called when the user enters an invalid entry
        "Invalid entry. Ending the game..."]
    print(endtext[parameter])

    raise SystemExit()

# Takes in the user name and prints introductery message


def intro():
    x = ""
    name = options(0)
    nametxt = ["Nice to meet you, ", name, "!", " Welcome to Independence, Missouri!"]
    for w in nametxt:
        x = x + w
    print(x)

# Asks the user if they want to start the game


def start():
    go = options(1)

    if go == "y":
        print("Alright, let's start the journey...")
    else:
        endgame(0)

# Asks the user whether they want to spend their money on weapons or clothes


def purchase():
    money = options(2)
    if money == "y":
        equipment = "clothes"
    elif money == "x":
        equipment = "weapons"
    else:
        endgame(7)

    return equipment


# Asks the user if they want to trade with the indians or continue
def indians():
    trade = options(3)
    return trade

# Asks the user if they to cross the river or wait for a better time


def river():
    crossriver = options(4)
    if crossriver == "y":
        endgame(1)
    elif crossriver == "x":
        print("You are able to cross after 5 days!")
    else:
        endgame(7)


# User meets robbers and decides whether to fight back or accept their demands
# Decides whether user survives if they fight back based on weapons and chance
def robbers(equipment):

    fight = options(7)
    if fight == "x":
        endgame(2)
    elif fight == "y":
        if equipment == "clothes" and chance() == 1:
            endgame(6)
        else:
            print("\n\nHurray, you defeated the robbers!")
    else:
        endgame(7)

# User reaches Sierra Nevada after 50 days


def sierra50():
    wait = options(8)
    return wait

# User reaches the Sierra Nevada after 60 days


def sierra60(equipment):
    second_wait = options(5)

    if second_wait == "y":
        if equipment == "weapons":
            if chance() == 1:
                endgame(3)
            elif chance() == 0:
                endgame(4)
        elif equipment == "clothes":
            endgame(3)
        else:
            endgame(7)
    elif second_wait == "x":
        return

    else:
        endgame(7)


# User reaches Sierra Nevada after 70 days
def final(equipment):
    last_wait = options(6)
    if last_wait == "y":
        endgame(5)
    elif last_wait == "x":
        if equipment == "weapons":
            endgame(4)
        elif equipment == "clothes":
            if chance() == 1:
                endgame(3)
            elif chance() == 0:
                endgame(4)
        else:
            endgame(7)
    else:
        endgame(7)


# The main function for running the game
def app():
    intro()

    start()

    equipment = purchase()

    trade = indians()

    if trade == "y":
        river()
        sierra60(equipment)

    elif trade == "x":
        robbers(equipment)
        wait = sierra50()

        if wait == "y":
            endgame(3)
        elif wait == "x":
            sierra60(equipment)
        else:
            endgame(7)
    else:
        endgame(7)

    final(equipment)


if __name__ == '__main__':
    """ The __name__ hidden variable contains the string "__main__" only when you run it with `python shaheen.py`
    If you check it with an if statement, you can import these functions in another game without running the app()
    """
    app()
