Would you like to go on a hike?

Going off trail is not allowed, please follow the appropriate trailhead signs.

Come back when you're ready!

Wonderful! You start your journey going up a tall and windy mountain.
All of a sudden, a hungry grizzly bear appears and starts racing towards you!
Do you attempt to climb a [tree] or tumble down a hill towards a [lake] in the distance?

Unfortunately, bears are talented tree climbers and you become its lunch, please try again.

You run away as fast as you can and barely escape the enraged grizzly.
You walk on and eventually stumble upon a small cabin right beside a waterfall.
Night is fast approaching and you need a place to sleep for the night.
Do you search the [cabin] or do you choose to investigate the [waterfall]?

You knock on the door and find that the door was unlocked.
You let yourself in and see a group of stone-trolls who (lucky for you) are sleeping.
Do you go [awaken] the trolls to ask for help or secretly [search] the cabin?

You search the cabin but find nothing to contact or lead you to the outside world.
Suddenly, you turn around and the trolls are all staring at you, furious at an intruder.
You should have been a more honest guest, please try again.

You make your way over to the trolls and they abruptly awaken and ask, "Who are you?"
You explain your situation and the trolls understand as the bear has attacked them too.
The trolls pack your bag with more supplies and helps you find the trail once again.
Do you [continue] and finish the trail or [leave] before to avoid more danger?

You try to leave the trail but unknown to you, the bear was waiting for you.
Though you escaped the bear once, you weren't so fortunate this time, please try again.

You go along and finish the hike with no more danger to be seen.
You later realize the trolls even gifted you an emerald charm to remember them by.
Even after all the danger, you smile knowing the hike was still very much worth it.

You investigate the waterfall and discover behind it a perfect place to sleep, a hidden cave.
You enter the cave and are greeted by a monstrous Yeti.
Do you run [deeper] into the cave or do you run [outside] in hopes to escape?

You attempt to go further into the cave but it's so dark that you trip and fall.
The yeti easily finds you, ending your adventure, please try again.

You rush outside and lucky for you, the yeti does not think you are worth its time.
With nowhere else to go, you must approach the cabin if you want to ever go home again.